module gitlab.com/metakeule/short

go 1.24.0

require (
	github.com/gdamore/tcell v1.1.0
	github.com/lithammer/fuzzysearch v1.0.1
	github.com/mattn/go-runewidth v0.0.16
	github.com/shurcooL/go-goon v1.0.0
	github.com/stretchr/powerwalk v0.0.0-20151124150408-bceb9d014549
	gitlab.com/golang-utils/config/v2 v2.15.3
	gitlab.com/golang-utils/fmtdate v1.0.2
	gitlab.com/golang-utils/pad v0.0.2
	gitlab.com/golang-utils/web v0.11.1
	gitlab.com/metakeule/pager v2.0.0+incompatible
	gitlab.com/metakeule/static-junk-server v0.1.4
	mvdan.cc/sh/v3 v3.10.0
)

require (
	github.com/charmbracelet/colorprofile v0.2.3-0.20250311203215-f60798e515dc // indirect
	github.com/charmbracelet/x/ansi v0.8.0 // indirect
	github.com/charmbracelet/x/cellbuf v0.0.13 // indirect
	github.com/charmbracelet/x/term v0.2.1 // indirect
	github.com/erikgeiser/coninput v0.0.0-20211004153227-1c3628e74d0f // indirect
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect
	github.com/schollz/progressbar/v3 v3.18.0 // indirect
	github.com/shurcooL/go v0.0.0-20200502201357-93f07166e636 // indirect
	github.com/xo/terminfo v0.0.0-20220910002029-abceb7e1c41e // indirect
	gitlab.com/golang-utils/httpgzip v0.0.3 // indirect
	gitlab.com/golang-utils/isnil v1.0.1 // indirect
	golang.org/x/net v0.37.0 // indirect
)

require (
	github.com/atotto/clipboard v0.1.4 // indirect
	github.com/aymanbagabas/go-osc52/v2 v2.0.1 // indirect
	github.com/charmbracelet/bubbles v0.20.0 // indirect
	github.com/charmbracelet/bubbletea v1.3.4 // indirect
	github.com/charmbracelet/lipgloss v1.1.0 // indirect
	github.com/donseba/go-htmx v1.11.3 // indirect
	github.com/emersion/go-appdir v1.1.2 // indirect
	github.com/gdamore/encoding v0.0.0-20151215212835-b23993cbb635 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-localereader v0.0.1 // indirect
	github.com/metakeule/pager v2.0.0+incompatible // indirect
	github.com/moby/sys/mountinfo v0.7.2 // indirect
	github.com/muesli/ansi v0.0.0-20230316100256-276c6243b2f6 // indirect
	github.com/muesli/cancelreader v0.2.2 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.16.0 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/smartystreets/goconvey v1.8.1 // indirect
	github.com/zs5460/art v0.3.0 // indirect
	gitlab.com/golang-utils/dialog v0.2.2 // indirect
	gitlab.com/golang-utils/errors v0.0.3
	gitlab.com/golang-utils/fs v0.22.1
	gitlab.com/golang-utils/scaffold v1.19.1 // indirect
	gitlab.com/golang-utils/updatechecker v0.1.4 // indirect
	gitlab.com/golang-utils/version v1.0.4 // indirect
	gitlab.com/metakeule/places v1.5.0 // indirect
	gitlab.com/metakeule/templ v0.4.0 // indirect
	golang.org/x/sync v0.12.0 // indirect
	golang.org/x/sys v0.31.0 // indirect
	golang.org/x/term v0.30.0 // indirect
	golang.org/x/text v0.23.0 // indirect
)
