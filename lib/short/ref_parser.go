package short

import (
	"fmt"
	"regexp"
	"strings"
)

type RefKey [2]string // first is the reference, second is the operator to the next (might be empty, or ";", or "&&" or "||")

var refMixRegStr = "(:[^" + regexp.QuoteMeta(":;&|") + "]+)(;|(" + regexp.QuoteMeta("&&") + ")|(" + regexp.QuoteMeta("||") + "))?"
var refMixReg = regexp.MustCompile(refMixRegStr)

func IsRef(mixed string) bool {
	if len(mixed) == 0 {
		return false
	}
	return mixed[0] == ':'
}

func IsValidRefKey(mixed string) bool {
	return refMixReg.MatchString(mixed)
}

func ParseRefKey(mixed string) (rks []RefKey, err error) {
	if !IsValidRefKey(mixed) {
		return nil, fmt.Errorf("not a ref key: %q", mixed)
	}

	submatches := refMixReg.FindAllStringSubmatch(mixed, -1)

	for _, sub := range submatches {
		var rk RefKey
		rk[0] = strings.TrimSpace(sub[1])
		rk[1] = strings.TrimSpace(sub[2])

		if len(rk[0]) == 0 {
			return nil, fmt.Errorf("not a ref key: %q", rk[0])
		}

		if rk[0][0] != ':' {
			return nil, fmt.Errorf("not a ref key: %q", rk[0])
		}

		switch rk[1] {
		case "", "||", "&&", ";":
		default:
			return nil, fmt.Errorf("wrong operator: %q", rk[1])
		}
		rks = append(rks, rk)
	}

	return rks, nil
}
