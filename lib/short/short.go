package short

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/golang-utils/fmtdate"
)

/*
this package takes a table/data-driven approach to something parametrized shell aliases (called "short.Cut").
*/

func SortByName(c Cuts, up bool) {
	sort.Slice(c, func(i, j int) bool {
		if up {
			return c[i].Name < c[j].Name
		}
		return c[j].Name < c[i].Name
	})
}

func SortByCommand(c Cuts, up bool) {
	sort.Slice(c, func(i, j int) bool {
		if up {
			return c[i].Command < c[j].Command
		}
		return c[j].Command < c[i].Command
	})
}

type Cut struct {
	Name    string `json:"name"`    // names may contain dots to create namespaces (to allow grouping)
	Command string `json:"command"` // a command for the shell. placeholders are defined by inserting something like #Name: Type#, where
	// Name is the name of the parameter and Type is one of string, int, uint, float, date, time
	// placeholders may have defaults and are asked when the shortcut is executed
	// if command starts with a colon, then the followong name is the name of the parent command of which this command is a special case
	Defaults map[string]string `json:"defaults"` // default values
	Timeout  string            `json:"timeout"`  // will be parsed via time.ParseDuration()
}

func (c Cut) IsValid() bool {
	if strings.Contains(c.Name, " ") {
		return false
	}

	if strings.Contains(c.Name, ":") {
		return false
	}

	if IsRef(c.Command) {
		return IsValidRefKey(c.Command)
	}

	return true
}

var Types = []string{"string", "int", "uint", "float", "date", "time", "bool"}

const ParamRegExp = `#([.-_a-z]+):([a-z]+)#`

var regexParamDef = regexp.MustCompilePOSIX(ParamRegExp)

func findParams(command string) [][]string {
	return regexParamDef.FindAllStringSubmatch(command, -1)
}

func ReplaceParamsInCommand(command string, params map[string]string) string {
	//	Write(fmt.Sprintf("replace in command %v with params: %#v\n", command, params))
	found := findParams(command)
	done := map[string]bool{}

	for i := 0; i < len(found); i++ {
		repl := found[i][0]
		key := found[i][1]
		if _, has := params[key]; has && !done[repl] {
			command = strings.Replace(command, repl, params[key], -1)
			done[repl] = true
		}
	}

	return command
}

func Params(cutName string, allCuts map[string]Cut) (paramsDefinition map[string]string, err error) {
	c := allCuts[cutName]

	if IsRef(c.Command) {
		refkeys, err := ParseRefKey(c.Command)
		if err != nil {
			return nil, fmt.Errorf("no valid reference: %q", c.Command)
		}

		if len(refkeys) == 1 {
			return Params(c.Command[1:], allCuts)
		}

		//	fmt.Printf("%#v\n", refkeys)

		pd := map[string]string{}

		cb := func(ct Cut, rk RefKey, params map[string]string) error {
			pd2, err2 := Params(rk[0][1:], allCuts)
			if err2 != nil {
				return err2
			}

			pd = finalValues(pd, pd2)
			return nil
			//return ct.execCommand(allCuts, params)
		}

		err = c.refkeys(refkeys, allCuts, map[string]string{}, cb)
		if err != nil {
			return nil, err
		}

		return pd, nil
	}

	cmd, _, err2 := c.commandAndValues(allCuts)

	if err2 != nil {
		err = err2
		return
	}

	var errors map[string]string
	paramsDefinition, errors = findParamsInCommand(cmd)

	if len(errors) > 0 {
		err = fmt.Errorf("ERROR in params definition: %v", errors)
	}
	return
}

func findParamsInCommand(command string) (paramsDefinition map[string]string, errors map[string]string) {
	paramsDefinition = map[string]string{}
	errors = map[string]string{}
	// the same param may appear several times, but only with the same type
	found := findParams(command)

	for i := 0; i < len(found); i++ {
		key := found[i][1]
		val := found[i][2]
		validType := false
		for _, t := range Types {
			if t == val {
				validType = true
			}
		}

		if !validType {
			errors[key] = "invalid type '" + val + "' for parameter '" + key + "'"
			continue
		}

		if v, has := paramsDefinition[key]; has {
			if val != v {
				errors[key] = "non matching type definitions for parameter '" + key + "': " + "'" + v + "' vs '" + val + "'"
			}
			continue
		}

		paramsDefinition[key] = val

	}
	return
}

func ValidateValues(command string, params map[string]string) error {
	pDef, defErr := findParamsInCommand(command)

	if len(defErr) > 0 {
		return fmt.Errorf("error in definition of command %#v: %v", command, defErr)
	}

	for k, v := range params {
		def, has := pDef[k]
		if !has {
			continue
			//return fmt.Errorf("parameter '%s' does not exist in command %#v", k, command)
		}

		switch def {
		case "bool":
			_, err := strconv.ParseBool(v)
			if err != nil {
				return fmt.Errorf("parameter '%s' is not a bool", k)
			}
		case "string":
		case "int":
			_, err := strconv.Atoi(v)
			if err != nil {
				return fmt.Errorf("parameter '%s' is not an int", k)
			}
		case "uint":
			i, err := strconv.Atoi(v)
			if err != nil || i < 0 {
				return fmt.Errorf("parameter '%s' is not an uint", k)
			}
		case "float":
			_, err := strconv.ParseFloat(v, 64)
			if err != nil {
				return fmt.Errorf("parameter '%s' is not a float", k)
			}
		case "date":
			_, err := fmtdate.ParseDate(v)
			if err != nil {
				return fmt.Errorf("parameter '%s' is not a date", k)
			}
		case "time":
			_, err := fmtdate.ParseTime(v)
			if err != nil {
				return fmt.Errorf("parameter '%s' is not a time", k)
			}
		default:
			panic("unreachable") // should have been checked via findParamsInCommand
		}

	}

	return nil
}

// valueGroups is a slice of values that is applied in order in that the value in the last map never overwrite
// previous values of the same key. This is to have multiple defaults like with parent Cuts
// the given command is expected to be no parent link, but the command of the final Cut
// the valueGroups should be ordered in such a way, that the first map is the parameters passed at runtime,
// the next the defaults for the chosen cut, the next the defaults of its parent and so on until the final Cut
// that has no parents
func finalValues(valueGroups ...map[string]string) (finals map[string]string) {

	finals = map[string]string{}

	for _, vals := range valueGroups {
		if vals != nil {
			for k, v := range vals {
				if _, has := finals[k]; !has {
					finals[k] = v
				}
			}
		}

	}

	return
}

type Cuts []Cut

func (c Cuts) Len() int {
	return len(c)
}

func (c Cuts) Swap(a, b int) {
	c[a], c[b] = c[b], c[a]
}

func Sort(allCuts map[string]Cut) (cs Cuts) {
	for _, c := range allCuts {
		cs = append(cs, c)
	}

	sort.Sort(cs)
	return
}

func (c Cuts) Less(a, b int) bool {
	return c[a].Name < c[b].Name
}

func (ct Cut) execCommand(allCuts map[string]Cut, runtimeParams map[string]string) error {

	cmd, err := ct.FullCommand(allCuts, runtimeParams)

	if err != nil {
		return err
	}

	return ExecCommandBash(cmd, ct.Timeout)

	/*
		c := ExecCommand(cmd)
		//c := exec.Command("/bin/sh", "-c", cmd)
		c.Stdin = os.Stdin
		c.Stdout = os.Stdout
		c.Stderr = os.Stderr
		return c.Run()
	*/
}

func (ct Cut) printCommand(allCuts map[string]Cut, runtimeParams map[string]string, wr io.Writer) error {
	cmd, err := ct.FullCommand(allCuts, runtimeParams)

	if err != nil {
		return err
	}

	_, err = fmt.Fprint(wr, cmd)
	return err
}

func (ct Cut) refkeys(refkeys []RefKey, allCuts map[string]Cut, runtimeParams map[string]string, cb func(c Cut, rk RefKey, params map[string]string) error) error {
	var k, op string
	var err error

	for _, rk := range refkeys {
		k = rk[0]

		c, has := allCuts[k[1:]]

		if !has {
			return fmt.Errorf("unknown reference: %#v", k)
		}

		switch op {
		case "", ";":
			err = cb(c, rk, finalValues(runtimeParams, ct.Defaults))
		case "&&":
			if err != nil {
				return err
			}
			err = cb(c, rk, finalValues(runtimeParams, ct.Defaults))
		case "||":
			if err != nil {
				err = cb(c, rk, finalValues(runtimeParams, ct.Defaults))
			}
		}
		op = rk[1]
	}

	return err
}

func Find(cutName string, allCuts map[string]Cut) (*Cut, error) {
	c, has := allCuts[cutName]

	if !has {
		return nil, fmt.Errorf("unknown: %#v", cutName)
	}

	return &c, nil
}

func Write(s string) {
	f, err := os.OpenFile("short.log", os.O_APPEND|os.O_CREATE, 0644)

	if err != nil {
		return
	}

	defer f.Close()

	fmt.Fprint(f, s)
	// ioutil.WriteFile(log)
}

func Defaults(cutName string, allCuts map[string]Cut) (cmd string, defaults map[string]string, err error) {
	c, err := Find(cutName, allCuts)

	if err != nil {
		return "", nil, err
	}

	// fmt.Printf("c.Name: %q c.Command: %q\n", c.Name, c.Command)
	var refkeys []RefKey

	if IsRef(c.Command) {
		//	Write(fmt.Sprintf("IsRef(%q) is true\n", c.Command))
		refkeys, err = ParseRefKey(c.Command)
		if err != nil {
			return "", nil, fmt.Errorf("no valid reference: %q", c.Command)
		}

		//Write(fmt.Sprintf("refkeys: %#v\n", refkeys))

		if len(refkeys) == 1 {
			cmd, vals, err2 := c.CommandAndValues(allCuts, map[string]string{})
			_ = cmd
			if err2 != nil {
				return "", nil, err2
			}
			return cmd, vals, nil
			//return c. c.printCommand(allCuts, runtimeParams, wr)
		}

		defaults = map[string]string{}
		cmds := ""
		//	fmt.Printf("%#v\n", refkeys)

		cb := func(ct Cut, rk RefKey, params map[string]string) error {
			//cmd2, vals2, err3 := ct.CommandAndValues(allCuts, map[string]string{})
			cmd2, vals2, err3 := ct.CommandAndValues(allCuts, params)
			cmds += cmd2 + rk[1]
			if err3 != nil {
				return err3
			}
			//	Write(fmt.Sprintf("vals2: %#v\n", vals2))
			//	Write(fmt.Sprintf("defaults: %#v\n", defaults))
			defaults = finalValues(defaults, vals2)
			//	Write(fmt.Sprintf("finalValues: %#v\n", defaults))
			return nil
		}

		err4 := c.refkeys(refkeys, allCuts, map[string]string{}, cb)
		if err4 != nil {
			return "", nil, err4
		}

		return cmds, defaults, nil
	}

	cmd3, vals4, err5 := c.CommandAndValues(allCuts, map[string]string{})
	_ = cmd3
	if err5 != nil {
		return "", nil, err5
	}
	return cmd3, vals4, nil
}

func Print(cutName string, allCuts map[string]Cut, runtimeParams map[string]string, wr io.Writer) (err error) {
	c, err := Find(cutName, allCuts)

	if err != nil {
		return err
	}

	//fmt.Printf("c.Name: %q c.Command: %q\n", c.Name, c.Command)
	var refkeys []RefKey

	if IsRef(c.Command) {
		refkeys, err = ParseRefKey(c.Command)
		if err != nil {
			return fmt.Errorf("no valid reference: %q", c.Command)
		}

		if len(refkeys) == 1 {
			return c.printCommand(allCuts, runtimeParams, wr)
		}

		//	fmt.Printf("%#v\n", refkeys)

		cb := func(ct Cut, rk RefKey, params map[string]string) error {
			err := ct.printCommand(allCuts, params, wr)
			if err != nil {
				return err
			}
			fmt.Fprint(wr, rk[1])
			return nil
		}

		return c.refkeys(refkeys, allCuts, runtimeParams, cb)
	}

	return c.printCommand(allCuts, runtimeParams, wr)
}

func Exec(cutName string, timeout string, allCuts map[string]Cut, runtimeParams map[string]string) (err error) {
	//fmt.Printf("cutName: %q runtimeParams: %#v\n", cutName, runtimeParams)

	c, err := Find(cutName, allCuts)

	if err != nil {
		return err
	}

	if timeout != "" {
		c.Timeout = timeout
	}

	//fmt.Printf("c.Name: %q c.Command: %q\n", c.Name, c.Command)
	var refkeys []RefKey

	if IsRef(c.Command) {
		refkeys, err = ParseRefKey(c.Command)
		if err != nil {
			return fmt.Errorf("no valid reference: %q", c.Command)
		}

		if len(refkeys) == 1 {
			return c.execCommand(allCuts, runtimeParams)
		}

		//	fmt.Printf("%#v\n", refkeys)

		cb := func(ct Cut, rk RefKey, params map[string]string) error {
			return ct.execCommand(allCuts, params)
		}

		return c.refkeys(refkeys, allCuts, runtimeParams, cb)

		//return c.execRefkeys(refkeys, allCuts, runtimeParams)
	}

	return c.execCommand(allCuts, runtimeParams)
}

func Add(c Cut, allCuts map[string]Cut) (err error) {
	var refkeys []RefKey

	if IsRef(c.Command) {
		refkeys, err = ParseRefKey(c.Command)
		if err != nil {
			return fmt.Errorf("no valid reference: %q", c.Command)
		}

		for _, rk := range refkeys {
			n := rk[0]
			if n == "" {
				return fmt.Errorf("no valid reference: %q", rk[0]+rk[1])
			}
			_, ok := allCuts[n[1:]]
			if !ok {
				return fmt.Errorf("unknown reference: %q", n)
			}
		}

	}

	allCuts[c.Name] = c
	//_, err := Command(c.Name, allCuts, nil) // validate all the default values
	//return err
	return nil
}

func Save(configJSON io.Writer, allCuts map[string]Cut) (err error) {
	var cuts []Cut

	for _, c := range allCuts {
		cuts = append(cuts, c)
	}

	b, err := json.MarshalIndent(cuts, "", "  ")
	if err != nil {
		return err
	}

	_, err = configJSON.Write(b)
	return
}

func Load(configJSON io.Reader) (allCuts map[string]Cut, err error) {

	var cuts []Cut
	err = json.NewDecoder(configJSON).Decode(&cuts)

	if err != nil {
		err = fmt.Errorf("invalid json: %s", err.Error())
		return
	}

	allCuts = map[string]Cut{}

	for _, c := range cuts {
		if _, has := allCuts[c.Name]; has {
			err = fmt.Errorf("more than one definition of %#v", c.Name)
			return
		}
		allCuts[c.Name] = c
	}

	return
}

func (c Cut) ResolveCommand(allCuts map[string]Cut) (resolved *Cut, appendix string, err error) {
	if len(c.Command) == 0 {
		return nil, "", fmt.Errorf("command can't be empty")
	}

	if c.Command[0] != ':' {
		return &c, "", nil
	}

	refname := c.Command[1:]

	idx := strings.Index(refname, " ")
	if idx > 0 {
		appendix = strings.TrimSpace(refname[idx:])
		refname = strings.TrimSpace(refname[:idx])
	}

	res, has := allCuts[refname]

	if !has {
		return nil, "", fmt.Errorf("unknown reference: %#v, defined for %#v", refname, c.Name)
	}

	return &res, appendix, nil
}

func (c Cut) commandAndValues(allCuts map[string]Cut) (cmd string, vals []map[string]string, err error) {
	vals = []map[string]string{c.Defaults}

	if c.Command[0] != ':' {
		cmd = c.Command
		return
	}

	c2, appendix, err := c.ResolveCommand(allCuts)

	if err != nil {
		return "", nil, err
	}

	var grps []map[string]string
	var res string
	res, grps, err = c2.commandAndValues(allCuts)

	if err != nil {
		return
	}
	vals = append(vals, grps...)
	cmd = res
	if appendix != "" {
		cmd += " " + appendix

	}

	return
}

func (c Cut) CommandAndValues(allCuts map[string]Cut, runtimeParams map[string]string) (cmd string, vals map[string]string, err error) {
	var valueGroups []map[string]string
	cmd, valueGroups, err = c.commandAndValues(allCuts)
	if err != nil {
		return
	}

	vals = finalValues(append([]map[string]string{runtimeParams}, valueGroups...)...)
	err = ValidateValues(cmd, vals)
	return
}

func (c Cut) FullCommand(allCuts map[string]Cut, runtimeParams map[string]string) (cmd string, err error) {
	var vals map[string]string
	cmd, vals, err = c.CommandAndValues(allCuts, runtimeParams)
	if err != nil {
		return
	}
	return ReplaceParamsInCommand(cmd, vals), nil
}
