package short

import (
	"reflect"
	"testing"
)

func TestRefParser(t *testing.T) {
	tests := []struct {
		input   string
		err     bool
		results []RefKey
	}{
		{":t1", false, []RefKey{{":t1", ""}}},
		{":t1;:t2", false, []RefKey{{":t1", ";"}, {":t2", ""}}},
		{":t1;:t2;:p4", false, []RefKey{{":t1", ";"}, {":t2", ";"}, {":p4", ""}}},
		{":t1&&:t2&&:p4", false, []RefKey{{":t1", "&&"}, {":t2", "&&"}, {":p4", ""}}},
		{":t1||:t2||:p4", false, []RefKey{{":t1", "||"}, {":t2", "||"}, {":p4", ""}}},

		{" :t1 ", false, []RefKey{{":t1", ""}}},
		{" :t1 ; :t2", false, []RefKey{{":t1", ";"}, {":t2", ""}}},
		{" :t1 ; :t2 ; :p4 ", false, []RefKey{{":t1", ";"}, {":t2", ";"}, {":p4", ""}}},
		{" :t1 && :t2 && :p4 ", false, []RefKey{{":t1", "&&"}, {":t2", "&&"}, {":p4", ""}}},
		{" :t1 || :t2 || :p4 ", false, []RefKey{{":t1", "||"}, {":t2", "||"}, {":p4", ""}}},

		// 10
		{":&t1", true, nil},
		{":&t1;:&t2", true, nil},
		{":&t1;:&t2;:&p4", true, nil},
		{":&t1&&:&t2&&:&p4", true, nil},
		{":&t1||:&t2||:&p4", true, nil},
	}

	for i, test := range tests {
		submatches, err := ParseRefKey(test.input)

		if test.err && err == nil {
			t.Errorf("[%v] parseRefKey(%q) returned not error, but expected one", i, test.input)
		}

		if !test.err && err != nil {
			t.Errorf("[%v] parseRefKey(%q) returned error %v // expected no error", i, test.input, err)
		}

		if !reflect.DeepEqual(submatches, test.results) {
			t.Errorf("[%v] parseRefKey(%q) returned %#v // expected: %#v", i, test.input, submatches, test.results)
		}

	}
}
