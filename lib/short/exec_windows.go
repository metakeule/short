//go:build windows
// +build windows

package short

import (
	"os/exec"
	"syscall"
)

func ExecCommand(progWithArgs string) *exec.Cmd {
	cmd := exec.Command("powershell.exe",
		"/Command",
		//`"`+p.Program+`.exe `+p.Args+`"`,
		progWithArgs,
	)

	cmd.SysProcAttr = &syscall.SysProcAttr{
		CreationFlags: syscall.CREATE_NEW_PROCESS_GROUP, //  .CREATE_NEW_CONSOLE,
	}
	return cmd
}
