//go:build !windows
// +build !windows

package short

import (
	"os/exec"
)

func ExecCommand(progWithArgs string) *exec.Cmd {
	//return exec.Command("/bin/sh", "-c", "exec "+progWithArgs)
	return exec.Command("/bin/sh", "-c", progWithArgs)
}
