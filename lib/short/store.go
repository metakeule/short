package short

import (
	"fmt"
	"os"
	"strings"
	"sync"

	"gitlab.com/metakeule/short/web/tableurl"
)

type FileStore struct {
	filePath string
	mx       sync.Mutex
}

func NewFileStore(filePath string) *FileStore {
	return &FileStore{
		filePath: filePath,
	}
}

func (s *FileStore) File() string {
	return s.filePath
}

func (s *FileStore) Save(allCuts map[string]Cut) error {
	s.mx.Lock()
	defer s.mx.Unlock()
	f, err := os.Create(s.filePath)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	defer f.Close()

	return Save(f, allCuts)
}

var allowedFields = map[string]bool{
	"name":    true,
	"command": true,
}

var filterModes = map[string]bool{
	"is":          true,
	"contains":    true,
	"starts-with": true,
	"ends-with":   true,
}

func (s *FileStore) LoadFiltered(u tableurl.URL) (filtered map[string]Cut, err error) {
	all, err := s.Load()

	if err != nil {
		return nil, err
	}

	if u.FilterField == "" {
		return all, nil
	}

	var compareFn func(a, b string) bool

	switch u.FilterMode {
	case "is":
		compareFn = func(a, b string) bool { return a == b }
	case "starts-with":
		compareFn = strings.HasPrefix
	case "ends-with":
		compareFn = strings.HasSuffix
	case "contains":
		compareFn = strings.Contains
	default:
		return nil, fmt.Errorf("unknown filtermode: %q", u.FilterMode)
	}

	var checkFn func(c Cut) bool

	switch u.FilterField {
	case "name":
		checkFn = func(c Cut) bool {
			return compareFn(c.Name, u.FilterValue)
		}
	case "command":
		checkFn = func(c Cut) bool {
			return compareFn(c.Command, u.FilterValue)
		}
	default:
		return nil, fmt.Errorf("unknown filterfield: %q", u.FilterField)
	}

	filtered = map[string]Cut{}

	for _, c := range all {
		if checkFn(c) {
			filtered[c.Name] = c
		}
	}

	return filtered, nil

}

func (s *FileStore) Load() (allCuts map[string]Cut, err error) {
	s.mx.Lock()
	defer s.mx.Unlock()
	var f *os.File
	f, err = os.Open(s.filePath)
	if err != nil {
		if os.IsNotExist(err) {
			err = nil
			allCuts = map[string]Cut{
				"ls": {
					Name:     "ls",
					Command:  "ls",
					Defaults: map[string]string{},
				},
			}
			return
		}

		return
	}

	defer f.Close()

	return Load(f)
}
