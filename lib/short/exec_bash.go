package short

import (
	"context"
	"os"
	"strings"
	"time"

	"mvdan.cc/sh/v3/interp"
	"mvdan.cc/sh/v3/syntax"
)

// execute command as multios bash
func ExecCommandBash(progWithArgs string, timeout string) error {
	// see https://pkg.go.dev/mvdan.cc/sh/v3/interp
	file, _ := syntax.NewParser().Parse(strings.NewReader(progWithArgs), "")
	runner, _ := interp.New(
		// Use [interp.Interactive] to enable interactive shell defaults like expanding aliases.
		//	interp.Env(expand.ListEnviron("GLOBAL=global_value")),
		interp.StdIO(os.Stdin, os.Stdout, os.Stderr),
	)

	ctx := context.Background()

	if timeout != "" {
		tout, err := time.ParseDuration(timeout)

		if err != nil {
			return err
		}

		var cancelfn context.CancelFunc
		ctx, cancelfn = context.WithTimeout(ctx, tout)
		_ = cancelfn
	}

	return runner.Run(ctx, file)

}
