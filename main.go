package main

import (
	"fmt"
	"os"
	"path/filepath"
)

func shortcutFileDefault() string {
	home, err := os.UserHomeDir()
	if err != nil {
		panic("can't get user home dir: " + err.Error())
	}
	return filepath.Join(home, ".short.json")
}

func main() {
	err := New().Run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s", err.Error())
		os.Exit(1)
	}
}
