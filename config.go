package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/golang-utils/config/v2"
	"gitlab.com/golang-utils/pad"
	"gitlab.com/metakeule/short/lib/short"
	"gitlab.com/metakeule/short/web"
)

type Runner struct {
	Config *config.Config

	ArgName       config.StringGetter
	ArgValues     config.StringGetter
	ArgPrint      config.BoolGetter
	ArgTimeout    config.StringGetter
	ArgConfigFile config.FileGetter

	Set struct {
		Config     *config.Config
		ArgCommand config.StringGetter
	}

	List struct {
		Config    *config.Config
		ArgPrefix config.StringGetter
	}

	Rm struct {
		Config *config.Config
	}

	UI struct {
		Config *config.Config
	}

	Web struct {
		Config  *config.Config
		ArgHost config.StringGetter
		ArgPort config.IntGetter
	}
}

func New() *Runner {
	var r = &Runner{}

	r.Config = config.New("short",
		"short is shortcut tool for running commands.",
		config.Concept(`
Commandline tools are very powerful, but often we need only certain features and certain combinations of arguments that are hard to remember.
Sometimes also the values are hard to remember.
short stores your favourite commands as strings with placeholders. these placeholders may be parameters passed to the underlying command, but they can be anywhere.
- So you as a user decide how to parametrize your favorite commands or if at all.
- You define your own shortcuts to memoize. You can build a hierarchy of shortcuts by putting dots between the names.
- And you can define default values for your parameters that you can overwrite on execution time if needed.
- And you can define shortcuts based on already defined shortcuts and "inherit" the placeholders, e.g. define more general shortcuts and more specific ones (with different defaults) based on them.
- Last but not least you can define shortcuts based on a set of other shortcuts whith sharing and overlapping parameters!

Syntax

A placeholder starts with a hash # followed by a name and a colon : followed by a type and a hash.
Examples:
  #host:string#
  #port:int#

Names must not have spaces or special characters. Types are string, int, uint, float, date, time, bool.
When the value is set, it is checked, if the type is correct.
The command is just a string as you would type it if you want to run the command in the shell. Only that it can have this placeholders where you want them.

You should always define defaults for your placeholders, so that you don't need to pass them.

If a shortcut is based on existing shortcuts, it command string starts with a colon, followed by the name of the shortcut, e.g.
  :ssh

would be the command of a shortcut based on the existing shortcut ssh.

If you want to have a shortcut based on a combination of other shortcuts, you need an operator in between them.
There are three kind of operators:
- ; (semicolon) executes the following command no matter, if the previous one did return an error or not
- || (double pipe) executes the following command, if the previous one did return an error
- && (double and) executes the following command, if the previous one did not return an error

See Examples section for some examples.

To execute and fill the parameters you can use the CLI UI which also show interactively the resulting command.
	`),
	)

	r.ArgConfigFile = r.Config.File("store", "file to save the shortcuts (defaults to HOME/.short.json)")

	r.ArgName = r.Config.LastString("name",
		"name of the shortcut. may contain dots to seperate namespaces, e.g.\nssh.server1",
		config.Required())

	r.ArgValues = r.Config.String("values",
		"values / parameters of the shortcut. e.g. \nport=22,host=localhost",
		config.Shortflag('v'))

	r.ArgPrint = r.Config.Bool("print",
		"don't run it just print the resulting command", config.Shortflag('p'))

	r.ArgTimeout = r.Config.String("timeout",
		"timeout as duration for the command to run (e.g. 2h30m40s)", config.Shortflag('t'))

	r.Set.Config = r.Config.Command("set", "sets a shortcut")
	r.Set.ArgCommand = r.Set.Config.String("command",
		`command to be run via the shortcut; it might have placeholders.
Syntax
- general
  ssh -p #port:int# root@#host:string#
- based on another shortcut
  :ssh
- combi of shortcuts being run in order
  :mv ; :scp ; :ls
- run only if previous succeeded
  :mv && :scp && :ls
- run only if the previous did not succeed
  :mv || :scp || :ls
- run in different conditions
  :mv ; :scp || :echo.failed
	`,
		config.Required(), config.Shortflag('c'))

	r.List.Config = r.Config.Command("ls", "lists all shortcuts").SkipAllBut("store")
	r.List.ArgPrefix = r.List.Config.LastString("prefix", "the prefix of the to be listed shortcuts")
	r.UI.Config = r.Config.Command("ui", "ui to run shortcuts").SkipAllBut("store")
	r.Rm.Config = r.Config.Command("rm", "removes a shortcut").SkipAllBut("store", "name")

	r.Web.Config = r.Config.Command("web", "runs a web server for the administration of the shortcuts (unprotect, use it only locally)").SkipAllBut("store")
	r.Web.ArgHost = r.Web.Config.String("host", "the hostname that the webserver should listen to", config.Default("localhost"))
	r.Web.ArgPort = r.Web.Config.Int("port", "port on which the webserver should listen. if the port number is negative, the next free port starting from the positive number will be used (10 tries)", config.Default(-8080))
	return r
}

func (r *Runner) mkStore() *short.FileStore {
	if r.ArgConfigFile.IsSet() {
		return short.NewFileStore(r.ArgConfigFile.Get().ToSystem())
	}

	return short.NewFileStore(shortcutFileDefault())
}

func (r *Runner) runWeb() error {
	st := r.mkStore()

	return web.Run(st, r.Web.ArgHost.Get(), r.Web.ArgPort.Get())
}

func (r *Runner) runSet() error {
	st := r.mkStore()
	all, err := st.Load()
	if err != nil {
		return err
	}

	var c short.Cut
	c.Name = r.ArgName.Get()
	c.Command = r.Set.ArgCommand.Get()
	c.Timeout = r.ArgTimeout.Get()
	c.Defaults, err = paramsStringToMap(r.ArgValues.Get())
	if err != nil {
		return err
	}

	if !c.IsValid() {
		return fmt.Errorf("invalid syntax")
	}

	if r.ArgPrint.Get() {
		fmt.Fprintf(os.Stdout, `would create the following short.Cut: 
	name: %s
	command: %s
	timeout: %v
	default values: %v
`, c.Name, c.Command, c.Timeout, c.Defaults)
		return nil
	}

	err = short.Add(c, all)

	if err != nil {
		return err
	}

	return st.Save(all)
}

func (r *Runner) runCmd() error {
	st := r.mkStore()
	all, err := st.Load()
	if err != nil {
		return err
	}

	var runtimeParams map[string]string
	runtimeParams, err = paramsStringToMap(r.ArgValues.Get())
	if err != nil {
		return err
	}

	if r.ArgPrint.Get() {
		c, err := short.Find(r.ArgName.Get(), all)
		if err != nil {
			return err
		}

		cmd, err := c.FullCommand(all, runtimeParams)

		if err != nil {
			return err
		}

		fmt.Fprint(os.Stdout, cmd+"\n")
		return nil
	}

	return short.Exec(r.ArgName.Get(), r.ArgTimeout.Get(), all, runtimeParams)
}

func (r *Runner) runList() error {
	st := r.mkStore()
	all, err := st.Load()
	if err != nil {
		return err
	}

	cuts := short.Sort(all)

	if len(cuts) == 0 {
		fmt.Fprintf(os.Stdout, "no shortcuts defined")
		return nil
	}

	if r.List.ArgPrefix.IsSet() {
		prefix := r.List.ArgPrefix.Get()

		var filtered short.Cuts
		for _, c := range cuts {
			if strings.HasPrefix(c.Name, prefix) {
				filtered = append(filtered, c)
			}
		}

		cuts = filtered
	}

	longest := findLongestName(cuts)

	for _, c := range cuts {
		left := pad.RightPad(c.Name, longest+2)
		fmt.Fprintf(os.Stdout, "%s -> %s\n", left, c.Command)
	}

	return nil
}

func findLongestName(cs short.Cuts) (longest int) {
	for _, c := range cs {
		w := pad.Width(c.Name)
		if w > longest {
			longest = w
		}
	}

	return
}

func (r *Runner) runRm() error {
	st := r.mkStore()
	all, err := st.Load()
	if err != nil {
		return err
	}

	name := strings.TrimSpace(r.ArgName.Get())
	if name == "" {
		return fmt.Errorf("empty name not allowed")
	}

	delete(all, name)

	return st.Save(all)
}

func (r *Runner) runUI() error {
	s, err := NewScreen(r.mkStore())

	if err != nil {
		return err
	}

	return s.Run()
}

func (r *Runner) Run() error {
	err := r.Config.Run()

	if err != nil {
		return err
	}

	switch r.Config.ActiveCommand() {
	case r.Set.Config:
		return r.runSet()
	case r.List.Config:
		return r.runList()
	case r.Rm.Config:
		return r.runRm()
	case r.UI.Config:
		return r.runUI()
	case r.Web.Config:
		return r.runWeb()
	default:
		return r.runCmd()
	}
}
