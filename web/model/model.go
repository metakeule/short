package model

import (
	"fmt"
	"net/http"

	"gitlab.com/golang-utils/web/lib/wc"
	"gitlab.com/metakeule/short/lib/short"
	"gitlab.com/metakeule/short/web/tableurl"
)

var Store *short.FileStore

type Context struct {
	Store *short.FileStore
	*wc.CoreContext
}

func StartCtx(rt wc.Route, rw http.ResponseWriter, req *http.Request) *Context {
	return &Context{
		Store,
		wc.NewCoreContext(rt, rw, req),
	}
}

var allowedFields = map[string]bool{
	"name":    true,
	"command": true,
}

func GetList(list short.Cuts, u tableurl.URL) error {
	if _, ok := allowedFields[u.SortBy]; !ok {
		return fmt.Errorf("not allowed sort field: %q", u.SortBy)
	}

	switch u.SortBy {
	case "name":
		short.SortByName(list, u.SortUp)
	case "command":
		short.SortByCommand(list, u.SortUp)
	default:
		return fmt.Errorf("not allowed sort field: %q", u.SortBy)
	}

	return nil
}
