package view

import (
	"gitlab.com/golang-utils/web/lib/html"
	. "gitlab.com/golang-utils/web/lib/html/tag"
	"gitlab.com/golang-utils/web/lib/htmx"
	"gitlab.com/golang-utils/web/lib/htmx/htmx_modal"
	"gitlab.com/golang-utils/web/lib/wc/crudl"
	"gitlab.com/metakeule/short/web/args"
)

var Create = struct {
	View *crudl.View

	FormURL,
	ID html.Pos
}{
	nil,

	HTML("formurl").Position(),
	ID("id").Position(),
}

func init() {
	Create.View = Templates.View("create", htmx.
		FormPost(Create.FormURL.String(), true,
			ID(Create.ID.String()),
			showErrors.FormEvents(),
			DIV(Class("field"),
				LABEL(Class("label"), "name"),
				DIV(Class("control"),
					INPUT(
						Class("input"),
						showErrors.AttrOnInputResetValidation(),
						ATTR("type", "text", "name", args.FormName.Name(), "value", ""),
					),
				),
			),

			DIV(Class("field"),
				LABEL(Class("label"), "command"),
				DIV(Class("control"),
					INPUT(
						Class("input"),
						showErrors.AttrOnInputResetValidation(),
						ATTR("type", "text", "name", args.FormCommand.Name(), "value", ""),
					),
				),
			),

			errorsContainer,

			DIV(Class("field is-horizontal"),
				DIV(Class("field-body"),

					DIV(Class("field"),
						LABEL(Class("label"), " "),
						DIV(Class("control"),
							INPUT(
								Class("button is-info"),
								ATTR("type", "submit", "value", "save"),
							),
						),
					),

					DIV(Class("field"),
						LABEL(Class("label"), " "),
						DIV(Class("control"),
							INPUT(
								Class("button is-warning"),
								ATTR("type", "submit", "value", "cancel"),
								htmx_modal.AttrCloseOnClick,
							),
						),
					),
				),
			),
		),
	)
}
