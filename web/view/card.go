package view

import (
	"gitlab.com/golang-utils/web/lib/html"
	. "gitlab.com/golang-utils/web/lib/html/tag"
	"gitlab.com/golang-utils/web/lib/wc/crudl"
)

var Card = struct {
	View *crudl.View

	Title,
	Content,
	Footer html.Pos
}{
	nil,

	HTML("title").Position(),
	HTML("content").Position(),
	HTML("footer").Position(),
}

func init() {
	Card.View = Templates.View("card",
		DIV(
			Class("card is-shadowless"),
			HEADER(
				Class("card-header"),
				H1(
					Class("card-header-title title"),
					Card.Title,
				),
			),
			DIV(
				Class("card-content"),
				DIV(
					Class("content"),
					Card.Content,
				),
			),
			FOOTER(
				Class("card-footer"),
				Card.Footer,
			),
		),
	)
}
