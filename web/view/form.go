package view

import (
	. "gitlab.com/golang-utils/web/lib/html/tag"
	"gitlab.com/golang-utils/web/lib/htmx/htmx_form"
)

var showErrors = htmx_form.ShowErrors(ARTICLE(
	Class("message is-danger"),
))

var errorsContainer = showErrors.With(
	DIV(
		Class("message-body"),
	),
)
