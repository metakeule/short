package view

import (
	"gitlab.com/golang-utils/web/lib/html"
	. "gitlab.com/golang-utils/web/lib/html/tag"
	. "gitlab.com/golang-utils/web/lib/html/tag/short"
	"gitlab.com/golang-utils/web/lib/wc/crudl"
	"gitlab.com/metakeule/short/web/layout"
)

/*
optimierungen:

# platzhalter:

	- instanzierung ohne typ, e.g. var pos = Position("xyz")
		und escapen im template, e.g. DIV(pos.Text(), PRE(pos.HTML()))
*/

var List = struct {
	View *crudl.ListView

	SortByNameURL,
	SortByNameArrow,
	SortByCommandURL,
	SortByCommandArrow,
	Name,
	Command,
	Defaults,
	EditURL,
	DeleteURL,
	rows html.Pos
}{
	nil,

	HTML("theadsortbynameurl").Position(),
	HTML("theadsortbynamearrow").Position(),
	HTML("theadsortbycommandurl").Position(),
	HTML("theadsortbycommandarrow").Position(),
	Text("rowname").Position(),
	Text("rowcommand").Position(),
	Text("rowdefaults").Position(),
	HTML("rowediturl").Position(),
	HTML("rowdeleteurl").Position(),
	HTML("rows").Position(),
}

func init() {
	List.View = Templates.ListView("list-template",
		TR(
			TD(List.Name),
			TD(CODE(Class("p-1"), List.Command)),
			TD(PRE(Class("p-1"), List.Defaults)),
			TD(
				DIV(Class("dropdown is-hoverable"),
					DIV(Class("dropdown-trigger"),
						//	BUTTON(Class("button is-ghost"),
						BUTTON(Class("button"),
							ATTR(
								"aria-haspopup", "true",
								"aria-controls", "dropdown-menu1",
							),
							SPAN("action"),
							SPAN(
								Class("icon is-small"),
								I(
									Class("fas fa-angle-down"),
									ATTR("aria-hidden", "true"),
								),
							),
						),
					),
					DIV(Class("dropdown-menu"), ID("dropdown-menu1"), ATTR("role", "menu"),
						DIV(
							Class("dropdown-content"),
							layout.LAYOUT.Modals.HtmxSetFrom(List.EditURL.String()).Link().With(
								Class("dropdown-item"),
								SPAN(Class("icon is-small"),
									I(Class("fas fa-edit"),
										ATTR(
											"aria-hidden", "true",
										)),
								),
								SPAN(" edit "),
							),
							AHref(List.DeleteURL.String(),
								Class("dropdown-item"),
								SPAN(Class("icon is-small"),
									I(Class("fas fa-eraser"),
										ATTR(
											"aria-hidden", "true",
										)),
								),
								SPAN(" delete "),
							),
						),
					),
				),
			),
		),
		List.rows,
		TABLE(
			Class("table is-hoverable is-fullwidth is-narrow is-vcentered"),
			THEAD(
				TR(
					TH(
						AHref(
							List.SortByNameURL.String(),
							SPAN("name"),
							SPAN(Class("icon is-small"),
								I(Class("fas "+List.SortByNameArrow.String()), ATTR("aria-hidden", "true")),
							),
						),
					),
					TH(
						AHref(
							List.SortByCommandURL.String(),
							SPAN("command"),
							SPAN(Class("icon is-small"),
								I(Class("fas "+List.SortByCommandArrow.String()), ATTR("aria-hidden", "true")),
							),
						),
					),
					TH("defaults"),
					TH(),
				),
			),
			TBODY(List.rows),
		),
	)
}

func SortDirToArrow(selected, up bool) string {
	if !selected {
		return ""
	}

	if up {
		return "fa-arrow-up"
	}

	return "fa-arrow-down"
}
