package view

import (
	"embed"

	_ "github.com/shurcooL/go-goon"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/web/lib/wc/crudl"
)

//go:generate goexec "view.Templates.SaveAll()"

//go:embed cached_templates/*
var embedfsys embed.FS

var Templates = crudl.Templates(embedfsys, path.Relative("cached_templates/"))
