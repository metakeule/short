package view

import (
	"gitlab.com/golang-utils/web/lib/html"
	"gitlab.com/golang-utils/web/lib/html/attr"
	. "gitlab.com/golang-utils/web/lib/html/tag"
	"gitlab.com/golang-utils/web/lib/htmx"
	"gitlab.com/golang-utils/web/lib/htmx/htmx_modal"
	"gitlab.com/golang-utils/web/lib/wc/crudl"
	"gitlab.com/metakeule/short/web/args"
)

var Update = struct {
	View *crudl.ListView

	FormURL,
	ID,
	Name,
	Command,
	DefaultKey,
	DefaultValue,
	defaults html.Pos
}{
	nil,

	HTML("formurl").Position(),
	ID("id").Position(),
	Text("name").Position(),
	Text("command").Position(),
	Text("defaultkey").Position(),
	Text("defaultvalue").Position(),
	HTML("defaults").Position(),
}

func init() {
	Update.View = Templates.ListView("update",

		DIV(Class("field has-addons"),
			DIV(Class("control"),
				INPUT(Class("input"),
					attr.TypeText(),
					attr.Name(args.FormDefaultsKey.Name()),
					attr.Value(Update.DefaultKey.String()),
				//	attr.Disabled(),
				),
			),
			DIV(Class("control"),
				INPUT(Class("input"),
					attr.TypeText(),
					attr.Name(args.FormDefaultsValue.Name()),
					attr.Value(Update.DefaultValue.String()),
					showErrors.AttrOnInputResetValidation(),
				),
				//OnInputResetValidation,
			),
		),

		Update.defaults,

		htmx.FormPost(Update.FormURL.String(), true,
			ID(Update.ID.String()),
			//htmx.Attr_("on resetValidation add .is-hidden to .errors"),
			showErrors.FormEvents(),
			DIV(Class("field"),
				LABEL(Class("label"), "name"),
				DIV(Class("control"),
					INPUT(Class("input"),
						attr.TypeText(),
						attr.Name(args.FormName.Name()),
						attr.Value(Update.Name.String()),
						attr.Disabled(),
					),
				),
			),

			DIV(Class("field"),
				LABEL(Class("label"), "command"),
				DIV(Class("control"),
					INPUT(Class("input"),
						attr.TypeText(),
						attr.Name(args.FormCommand.Name()),
						attr.Value(Update.Command.String()),
						showErrors.AttrOnInputResetValidation(),
					),
					//OnInputResetValidation,
				),
			),
			DIV(Class("field"),
				LABEL(Class("label"), "defaults"),
				Update.defaults,
			),

			errorsContainer,

			DIV(Class("field is-horizontal"),
				DIV(Class("field-body"),
					DIV(Class("field"),
						LABEL(Class("label"), ""),
						DIV(Class("control"),
							INPUT(Class("button is-info"),
								attr.TypeSubmit(),
								attr.Value("save"),
							),
						),
					),

					DIV(Class("field"),
						LABEL(Class("label"), " "),
						DIV(Class("control"),
							INPUT(Class("button is-warning"),
								attr.TypeSubmit(),
								attr.Value("cancel"),
								htmx_modal.AttrCloseOnClick,
							),
						),
					),
				),
			),
		),
	)
}
