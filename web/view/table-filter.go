package view

import (
	"gitlab.com/golang-utils/web/lib/html"
	. "gitlab.com/golang-utils/web/lib/html/tag"
	. "gitlab.com/golang-utils/web/lib/html/tag/short"
	"gitlab.com/metakeule/short/web/tableurl"
)

var tableFilterViewPos = struct {
	ShowAllURL,
	FilterURL,
	SelectField,
	SelectMode,
	FilterValue html.Pos
}{
	HTML("show-all-url").Position(),
	HTML("filter-url").Position(),
	HTML("select-field").Position(),
	HTML("select-mode").Position(),
	Text("filter-value").Position(),
}

var tableFilterView = Templates.View("filter-view", DIV(
	Class("content"),
	DIV(
		Class("content"),
		FORM(
			ATTR("method", "get", "action", tableFilterViewPos.FilterURL.String()),
			DIV(
				Class("field is-horizontal"),
				DIV(
					Class("field-body"),

					DIV(
						Class("field"),
						DIV(
							Class("control"),

							AHref(
								tableFilterViewPos.ShowAllURL.String(),
								Class("button is-link is-light"),
								"show all",
							),
						),
					),
					DIV(
						Class("field"),
						DIV(
							Class("control"),
							DIV(
								Class("select"),
								tableFilterViewPos.SelectField.String(),
							),
						),
					),
					DIV(
						Class("field"),
						DIV(
							Class("control"),
							DIV(
								Class("select"),
								tableFilterViewPos.SelectMode.String(),
							),
						),
					),
					DIV(
						Class("field"),
						DIV(
							Class("control"),
							INPUT(
								Class("input"),
								ATTR(
									"type", "text",
									"placeholder", "value",
									"name", "filtervalue",
									"value", tableFilterViewPos.FilterValue.String(),
								),
							),
						),
					),
					DIV(
						Class("field"),
						DIV(
							Class("control"),
							INPUT(
								Class("button"),
								ATTR("type", "submit", "value", "search"),
							),
						),
					),
				),
			),
		),
	),
),
)

func TableFilter(u tableurl.URL) html.HTMLer {

	var (
		selectField = SELECT(ATTR("name", "filterfield"))
		optName     = OPTION("name")
		optCmd      = OPTION("command")
	)

	switch u.FilterField {
	case "name":
		optName.Add(ATTR("selected", "selected"))
	case "command":
		optCmd.Add(ATTR("selected", "selected"))
	}

	selectField.Add(optName, optCmd)

	var (
		selectMode  = SELECT(ATTR("name", "filtermode"))
		optEq       = OPTION("is")
		optContains = OPTION("contains")
		optStart    = OPTION("starts with", ATTR("value", "starts-with"))
		optEnd      = OPTION("ends with", ATTR("value", "ends-with"))
	)

	switch u.FilterMode {
	case "is":
		optEq.Add(ATTR("selected", "selected"))
	case "contains":
		optContains.Add(ATTR("selected", "selected"))
	case "starts-with":
		optStart.Add(ATTR("selected", "selected"))
	case "ends-with":
		optEnd.Add(ATTR("selected", "selected"))
	}

	selectMode.Add(optEq, optContains, optStart, optEnd)

	var showAll = u
	showAll.FilterField = ""
	showAll.FilterMode = ""
	showAll.FilterValue = ""
	showAll.Page = 1

	return tableFilterView.Set(
		tableFilterViewPos.ShowAllURL.Set(showAll.URL()),
		tableFilterViewPos.FilterURL.Set(u.URL()),
		tableFilterViewPos.SelectField.Set(selectField.HTML().String()),
		tableFilterViewPos.SelectMode.Set(selectMode.HTML().String()),
		tableFilterViewPos.FilterValue.Set(u.FilterValue),
	)
}
