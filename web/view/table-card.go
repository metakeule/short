package view

import (
	"fmt"

	"gitlab.com/golang-utils/web/lib/html"
	. "gitlab.com/golang-utils/web/lib/html/tag"
	. "gitlab.com/golang-utils/web/lib/html/tag/short"
	"gitlab.com/metakeule/short/web/layout"
	"gitlab.com/metakeule/short/web/tableurl"
)

var tableCardPos = struct {
	FilterForm,
	NoItems,
	CreateURL,
	PagingFormURL,
	LimitSelect,
	PaginationList,
	Table html.Pos
}{
	HTML("filterform").Position(),
	HTML("no_items").Position(),
	HTML("create_url").Position(),
	HTML("paging-form_url").Position(),
	HTML("limit-select").Position(),
	HTML("pagination-list").Position(),
	HTML("table").Position(),
}

var tableCardView = Templates.View("table-card",
	DIV(
		Class("card is-shadowless"),
		HEADER(
			Class("card-header"),
			H1(
				Class("card-header-title title"),
				"shortcut management",
			),
		),
		DIV(
			Class("card-content"),
			tableCardPos.FilterForm,
			DIV(
				Class("content"),
				DIV(
					Class("field is-grouped is-grouped-multiline"),
					DIV(
						Class("control"),
						DIV(
							Class("tags has-addons"),
							SPAN(
								Class("tag is-link"),
								fmt.Sprintf("%v", tableCardPos.NoItems),
							),
							SPAN(
								Class("tag is-dark"),
								"found",
							),
						),
					),
				),
				tableCardPos.Table,
			),
		),
		FOOTER(
			Class("card-footer"),
			DIV(
				Class("content column is-full"),
				NAV(
					Class("level"),
					DIV(
						Class("level-left"),
						DIV(
							Class("level-item"),
							P(
								Class("control"),
								layout.LAYOUT.Modals.HtmxSetFrom(tableCardPos.CreateURL.String()).Link().With(
									Class("button is-link"),
									"new shortcut",
								),
							),
						),
					),
					DIV(
						Class("level-right"),
						DIV(
							Class("level-item"),

							FORM(
								ATTR("method", "post", "action", tableCardPos.PagingFormURL.String()),
								SPAN(Class("field has-addons"),
									//LABEL(Class("label"), "items per page "),
									SPAN(Class("control"),
										SPAN(
											Class("select"),
											tableCardPos.LimitSelect,
										),
									),
									SPAN(Class("control"),
										BUTTON(Class("button is-link"), "page per item"),
									),
								),
							),
							NAV(
								Class("pagination"),
								ATTR(
									"role", "navigation",
									"aria-label", "pagination",
								),
								tableCardPos.PaginationList,
							),
						),
					),
				),
			),
		),
	),
)

func TableCard(createURL string, totalNo int, u tableurl.URL, table html.HTMLer) html.HTMLer {
	urlFn := func(page int) string {
		u.Page = page
		return u.URL()
	}

	return tableCard(
		TableFilter(u),
		totalNo,
		u,
		urlFn,
		table,
		createURL,
	)
}

func tableCard(filterform html.HTMLer, totalNo int, u tableurl.URL, pageURLFn func(page int) string, table html.HTMLer, createURL string) html.HTMLer {

	limitSelect := SELECT(
		ATTR("name", "limit"),
	)

	//limits := []int{10, 20, 50}
	limits := []int{5, 10, 2}

	for i := range limits {
		opt := OPTION(fmt.Sprintf("%v", limits[i]))
		if limits[i] == u.Limit {
			opt.Add(ATTR("selected", "selected"))
		}
		limitSelect.Add(opt)
	}

	return tableCardView.Set(
		tableCardPos.FilterForm.Set(filterform.HTML().String()),
		tableCardPos.NoItems.Setf("%v", totalNo),
		tableCardPos.Table.Set(table.HTML().String()),
		tableCardPos.CreateURL.Set(createURL),
		tableCardPos.PagingFormURL.Set(pageURLFn(u.Page)),
		tableCardPos.LimitSelect.Set(limitSelect.HTML().String()),
		tableCardPos.PaginationList.Set(paginationList(u, totalNo, pageURLFn).HTML().String()),
	)
}

func paginationList(u tableurl.URL, total int, urlFn func(page int) string) *html.Element {
	list := UL(Class("pagination-list"))

	numPages := total / u.Limit

	if total%u.Limit > 0 {
		numPages += 1
	}

	//fmt.Printf("numPages: %v\n", numPages)

	for i := 0; i < numPages; i++ {
		url := urlFn(i + 1)
		a := AHref(url)

		cl := Class("pagination-link")
		if i == u.Page-1 {
			a = A()
			cl = Class("pagination-link is-current")
			a.Add(ATTR(
				"aria-current", "page",
			))
		}

		a.Add(cl,
			ATTR("aria-label", fmt.Sprintf("Goto page %v", i+1)),
			fmt.Sprintf("%v", i+1),
		)

		list.Add(LI(a))
	}

	return list
}
