package view

import (
	"gitlab.com/golang-utils/web/lib/html"
	. "gitlab.com/golang-utils/web/lib/html/tag"
	"gitlab.com/golang-utils/web/lib/htmx/htmx_modal"
	"gitlab.com/golang-utils/web/lib/wc/crudl"
)

var Modal = struct {
	View    *crudl.View
	Content html.Pos
}{
	nil,
	HTML("content").Position(),
}

var (
	modalClass   = Class("modal")
	modalBGClass = Class("modal-background")
)

func init() {
	Modal.View = Templates.View("modal", DIV(
		htmx_modal.Events(modalClass.Selector(), modalBGClass.Selector()),
		modalClass,
		Class("is-active"),
		DIV(modalBGClass),
		DIV(Class("modal-content"), Modal.Content),
		BUTTON(Class("modal-close is-large"), ATTR("aria-label", "close"), htmx_modal.AttrCloseOnClick),
	),
	)
}
