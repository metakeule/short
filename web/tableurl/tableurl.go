package tableurl

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
)

type URL struct {
	DefaultSortUp bool
	DefaultSortBy string
	DefaultLimit  int
	SortBy        string
	SortUp        bool
	FilterField   string
	FilterMode    string
	FilterValue   string
	Page          int
	BaseURL       string
	Limit         int
}

func (u URL) Offset() int {
	page := u.Page
	if page == 0 {
		page = 1
	}

	limit := u.Limit

	if limit == 0 {
		limit = u.DefaultLimit
	}

	return (page - 1) * limit
}

// SortByURL returns the link for sorting by the given column
// if the column is the same as the current column, the sorting direction is reversed
// otherwise DefaultSortUp is used
func (u URL) SortByURL(sortby string) URL {
	u.Page = 1
	if u.SortBy == sortby {
		u.SortUp = !u.SortUp
		return u
	}
	u.SortBy = sortby
	u.SortUp = u.DefaultSortUp
	return u
}

func (u URL) URL() string {
	var ur url.URL
	ur.Path = u.BaseURL
	args := ur.Query()
	sortby := u.SortBy
	if sortby == "" {
		sortby = u.DefaultSortBy
	}
	args.Set("sortby", sortby)
	args.Set("sortup", fmt.Sprintf("%v", u.SortUp))
	args.Set("filterfield", u.FilterField)
	args.Set("filtermode", u.FilterMode)
	args.Set("filtervalue", u.FilterValue)
	page := u.Page
	if page == 0 {
		page = 1
	}
	args.Set("page", fmt.Sprintf("%v", page))
	limit := u.Limit
	if limit == 0 {
		limit = u.DefaultLimit
	}
	args.Set("limit", fmt.Sprintf("%v", limit))
	//args.
	ur.RawQuery = args.Encode()
	return ur.String()
}

func (u *URL) ScanPostLimit(req *http.Request) (err error) {
	err = u.Scan(req)
	if err != nil {
		return err
	}

	err = req.ParseForm()

	if err != nil {
		return err
	}

	formvals := req.PostForm
	//fmt.Printf("formvals: %#v\n", formvals)
	limit := formvals.Get("limit")
	if limit == "" {
		u.Limit = u.DefaultLimit
		return nil
	}

	u.Limit, err = strconv.Atoi(limit)
	if err != nil {
		return err
	}

	u.Page = 1

	return nil
}

func (u *URL) ScanPostFilter(req *http.Request) (err error) {
	err = u.Scan(req)
	if err != nil {
		return err
	}

	formvals := req.PostForm
	u.FilterField = formvals.Get("filterfield")
	u.FilterMode = formvals.Get("filtermode")
	u.FilterValue = formvals.Get("filtervalue")
	u.Page = 1
	return nil
}

func (u *URL) Scan(req *http.Request) (err error) {
	args := req.URL.Query()
	u.SortBy = args.Get("sortby")
	sortup := args.Get("sortup")
	if sortup != "" {
		u.SortUp, err = strconv.ParseBool(sortup)
		if err != nil {
			return err
		}
	}

	if u.SortBy == "" {
		u.SortBy = u.DefaultSortBy
		u.SortUp = u.DefaultSortUp
	}

	u.FilterField = args.Get("filterfield")
	u.FilterMode = args.Get("filtermode")
	u.FilterValue = args.Get("filtervalue")
	page := args.Get("page")
	if page != "" {
		u.Page, err = strconv.Atoi(page)
		if err != nil {
			return err
		}
	}

	if u.Page == 0 {
		u.Page = 1
	}

	limit := args.Get("limit")
	if limit != "" {
		u.Limit, err = strconv.Atoi(limit)
		if err != nil {
			return err
		}
	}

	if u.Limit == 0 {
		u.Limit = u.DefaultLimit
	}

	return nil
}
