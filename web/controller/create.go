package controller

import (
	"fmt"

	"gitlab.com/golang-utils/errors"
	"gitlab.com/golang-utils/web/lib/wc"
	"gitlab.com/golang-utils/web/lib/wc/crudl"
	"gitlab.com/metakeule/short/lib/short"
	"gitlab.com/metakeule/short/web/args"
	"gitlab.com/metakeule/short/web/model"
	v "gitlab.com/metakeule/short/web/view"
)

var Create = struct {
	Routes crudl.Create[*model.Context]

	FormName    wc.FormArg
	FormCommand wc.FormArg
}{
	crudl.NewCreate(Router, "short-create"),

	args.FormName,
	args.FormCommand,
}

func init() {
	Create.Routes.GET.
		Do(func(ctx *model.Context) (err error) {
			return ctx.SendHTML(
				v.Modal.View.Set(
					v.Modal.Content.Set(
						v.Card.View.Set(
							v.Card.Title.Set("create shortcut"),
							v.Card.Content.Set(
								v.Create.View.Set(
									v.Create.FormURL.Set(Create.Routes.POST.URL()),
									v.Create.ID.Set(Create.Routes.ID.String()),
								),
							),
							v.Card.Footer.Set(""),
						),
					),
				),
			)
		})

	Create.Routes.POST.
		WithArgs(Create.FormName, Create.FormCommand).
		Do(func(ctx *model.Context) (err error) {
			var name, command string
			err = ctx.Scan(
				&name,
				&command,
			)

			if err != nil {
				return
			}

			all, err := ctx.Store.Load()

			if err != nil {
				return err
			}

			_, has := all[name]

			if has {
				var errM = errors.ErrMap{}
				errM.Add(args.FormName.Name(), fmt.Errorf("%s already exists", name))
				return ctx.SendFormErrors(errM)
			}

			var c short.Cut
			c.Command = command
			c.Name = name
			c.Defaults = map[string]string{}

			err = short.Add(c, all)

			if err != nil {
				var errM = errors.ErrMap{}
				errM.Add("", err)
				return ctx.SendFormErrors(errM)
			}

			err = ctx.Store.Save(all)

			if err != nil {
				return err
			}

			return ctx.HxRefreshOrRedirect(List.Routes.GET.URL())
		})
}
