package controller

import (
	"fmt"
	"os"

	//	. "gitlab.com/golang-utils/web/lib/html/tag"
	"gitlab.com/golang-utils/web/lib/wc"
	"gitlab.com/metakeule/short/web/model"
)

var Router = wc.New("short", model.StartCtx)

func init() {
	Router.SetErrorHandler(func(r wc.Route, ctx *model.Context, err error) (cont bool) {
		fmt.Fprintf(os.Stderr, "ERROR in %s %s: %s\n", r.Method(), ctx.Request().URL.String(), err.Error())
		return true
	})
}
