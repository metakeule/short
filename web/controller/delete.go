package controller

import (
	"gitlab.com/golang-utils/web/lib/wc"
	"gitlab.com/golang-utils/web/lib/wc/crudl"
	"gitlab.com/metakeule/short/lib/short"
	"gitlab.com/metakeule/short/web/model"
)

var Delete = struct {
	Routes crudl.DeleteGET[*model.Context]

	PathName wc.PathArg
}{
	crudl.NewDeleteGET(Router, "/delete"),

	wc.NewPathArg("name"),
}

func init() {
	Delete.Routes.GET.
		WithArgs(Delete.PathName).
		Do(func(ctx *model.Context) (err error) {
			var name string
			err = ctx.Scan(&name)

			if err != nil {
				return
			}

			old, err := ctx.Store.Load()

			if err != nil {
				return err
			}

			new := map[string]short.Cut{}

			for k := range old {
				if k != name {
					new[k] = old[k]
				}
			}

			err = ctx.Store.Save(new)

			if err != nil {
				return err
			}

			return ctx.Redirect(List.Routes.GET.URL())
		})
}
