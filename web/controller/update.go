package controller

import (
	"fmt"

	"gitlab.com/golang-utils/errors"
	"gitlab.com/golang-utils/web/lib/wc"
	"gitlab.com/golang-utils/web/lib/wc/crudl"
	"gitlab.com/metakeule/short/lib/short"
	"gitlab.com/metakeule/short/web/args"
	"gitlab.com/metakeule/short/web/model"
	v "gitlab.com/metakeule/short/web/view"
)

var Update = struct {
	Routes crudl.UpdatePOST[*model.Context]

	PathName    wc.PathArg
	FormCommand wc.FormArg
}{
	crudl.NewUpdatePOST(Router, "update"),

	args.PathName,
	args.FormCommand,
}

func init() {

	Update.Routes.GET.
		WithArgs(Update.PathName).
		Do(func(ctx *model.Context) (err error) {
			var name string
			err = ctx.Scan(&name)

			if err != nil {
				return
			}

			all, err := ctx.Store.Load()

			if err != nil {
				return err
			}

			c, has := all[name]

			if !has {
				return ctx.StatusNotFound()
			}

			if c.Defaults == nil {
				c.Defaults = map[string]string{}
			}

			templ := v.Update.View.New()
			pdef, _ := short.Params(name, all)

			for k := range pdef {
				templ.Add(
					v.Update.DefaultKey.Set(k),
					v.Update.DefaultValue.Set(c.Defaults[k]),
				)
			}

			return ctx.SendHTML(
				v.Modal.View.Set(
					v.Modal.Content.Set(
						v.Card.View.Set(
							v.Card.Title.Set("edit shortcut"),
							v.Card.Content.Set(
								templ.Set(
									v.Update.FormURL.Set(
										Update.Routes.POST.URL(
											Update.PathName.Set(name),
										),
									),
									v.Update.ID.Set(Update.Routes.ID.String()),
									v.Update.Name.Set(c.Name),
									v.Update.Command.Set(c.Command),
								),
							),
							v.Card.Footer.Set(""),
						),
					),
				),
			)
		})

	Update.Routes.POST.
		WithArgs(Update.PathName, Update.FormCommand).
		Do(func(ctx *model.Context) (err error) {
			var (
				name string
				cmd  string
			)
			err = ctx.Scan(&name, &cmd)

			if err != nil {
				return
			}

			req := ctx.Request()

			defaultKeys := req.PostForm[args.FormDefaultsKey.Name()]
			defaultValues := req.PostForm[args.FormDefaultsValue.Name()]

			if len(defaultKeys) != len(defaultValues) {
				return fmt.Errorf("invalid defaults: number of keys must be equal to the number of values")
			}

			all, err := ctx.Store.Load()

			if err != nil {
				return err
			}

			c, has := all[name]

			if !has {
				return ctx.StatusNotFound()
			}

			c.Command = cmd
			c.Defaults = map[string]string{}
			for i := 0; i < len(defaultKeys); i++ {
				val := defaultValues[i]
				if val != "" {
					c.Defaults[defaultKeys[i]] = val
				}
			}

			res, _, err := c.ResolveCommand(all)

			if err != nil {
				errs := errors.ErrMap{}
				errs.Add(args.FormCommand.Name(), err)
				return ctx.SendFormErrors(errs)
			}

			err = short.ValidateValues(res.Command, c.Defaults)

			if err != nil {
				errs := errors.ErrMap{}
				errs.Add("", err)
				return ctx.SendFormErrors(errs)
			}

			all[name] = c

			err = ctx.Store.Save(all)

			if err != nil {
				return err
			}

			return ctx.HxRefreshOrRedirect(List.Routes.GET.URL())
		})

}
