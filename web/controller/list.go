package controller

import (
	"fmt"

	"gitlab.com/golang-utils/web/lib/wc/crudl"
	"gitlab.com/metakeule/short/lib/short"
	"gitlab.com/metakeule/short/web/layout"
	"gitlab.com/metakeule/short/web/model"
	"gitlab.com/metakeule/short/web/tableurl"
	"gitlab.com/metakeule/short/web/view"
)

var ListPostLimit = Router.POST("/list").
	Do(func(ctx *model.Context) (err error) {
		var u = NewTableURL()
		err = u.ScanPostLimit(ctx.Request())

		if err != nil {
			return
		}

		return ctx.Redirect(u.URL())
	})

var List = struct {
	Routes crudl.List[*model.Context]
}{
	crudl.NewList(Router, "/list"),
}

func init() {
	List.Routes.GET.
		Do(func(ctx *model.Context) (err error) {

			u := NewTableURL()
			err = u.Scan(ctx.Request())

			if err != nil {
				return
			}

			all, err := ctx.Store.LoadFiltered(u)

			if err != nil {
				return err
			}

			var cts short.Cuts

			for _, scut := range all {
				cts = append(cts, scut)
			}

			err = model.GetList(cts, u)

			if err != nil {
				return err
			}

			start := u.Offset()
			end := start + u.Limit // limit

			templ := view.List.View.New()

			for i, scut := range cts {
				if i >= start && i < end {
					defaultStr := ""

					for k, v := range scut.Defaults {
						defaultStr += fmt.Sprintf("%s: %s\n", k, v)
					}

					if len(defaultStr) > 2 {
						defaultStr = defaultStr[:len(defaultStr)-1]
					}

					editURL := Update.Routes.GET.URL(Update.PathName.Set(scut.Name))
					deleteURL := Delete.Routes.GET.URL(Delete.PathName.Set(scut.Name))

					templ.Add(
						view.List.Name.Set(scut.Name),
						view.List.Command.Set(scut.Command),
						view.List.Defaults.Set(defaultStr),
						view.List.EditURL.Set(editURL),
						view.List.DeleteURL.Set(deleteURL),
					)
				}
			}

			table := templ.Set(
				view.List.SortByNameURL.Set(u.SortByURL("name").URL()),
				view.List.SortByNameArrow.Set(view.SortDirToArrow(u.SortBy == "name", !u.SortByURL("name").SortUp)),
				view.List.SortByCommandURL.Set(u.SortByURL("command").URL()),
				view.List.SortByCommandArrow.Set(view.SortDirToArrow(u.SortBy == "command", !u.SortByURL("command").SortUp)),
			)

			return ctx.SendHTML(
				layout.Full(
					view.TableCard(
						Create.Routes.GET.URL(),
						len(all),
						u,
						table,
					),
				),
			)
		})
}

func NewTableURL() (u tableurl.URL) {
	u.DefaultSortBy = "name"
	u.DefaultSortUp = true
	u.DefaultLimit = 10
	u.BaseURL = List.Routes.GET.URL()
	return
}
