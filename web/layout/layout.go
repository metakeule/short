package layout

import (
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/web/lib/html"
	"gitlab.com/golang-utils/web/lib/html/attr"
	. "gitlab.com/golang-utils/web/lib/html/tag"
	. "gitlab.com/golang-utils/web/lib/html/tag/short"
	"gitlab.com/golang-utils/web/lib/htmx"
)

var (
	static       = path.MustLocal("/static/")
	css          = static.Join("css/")
	fontawesome5 = static.Join("fontawesome5/css/")
	js           = static.Join("js/")
)

var LAYOUTPos = struct {
	Content html.Pos
	Errors  html.Pos
	Header  html.Pos
	Footer  html.Pos
	Modals  html.Pos
}{
	html.HTML("content").Position(),
	html.HTML("errors").Position(),
	html.HTML("header").Position(),
	html.HTML("footer").Position(),
	html.HTML("modals").Position(),
}

var LAYOUT = struct {
	*htmx.Layout
	Content *htmx.Layout
	Errors  *htmx.Layout
	Header  *htmx.Layout
	Modals  *htmx.Layout
	Footer  *htmx.Layout
}{}

func initLayout() {
	errors := DIV(
		LAYOUTPos.Errors,
		Class("section has-background-light"),
	)

	content := SECTION(
		Class("section has-background-light"),
		LAYOUTPos.Content,
	)

	header := HEADER(
		Class("header"),
		LAYOUTPos.Header,
		NAV(
			Class("navbar"),
			ATTR(
				"role", "navigation",
				"aria-label", "main navigation",
			),
			DIV(
				Class("navbar-brand"),
				A(

					Class("navbar-burger"),
					ATTR(
						"role", "button",
						"aria-label", "menu",
						"aria-expanded", "false",
						"data-target", "navbarBasicExample",
					),
					SPAN(ATTR("aria-hidden", "true")),
				),
			),
			DIV(
				ID("navbarBasicExample"),
				Class("navbar-menu"),
			),
		),
	)

	footer := FOOTER(
		Class("footer"),
		LAYOUTPos.Footer,
	)

	modals := DIV(
		LAYOUTPos.Modals,
	)

	LAYOUT.Errors = htmx.NewLayout("errors", errors)

	LAYOUT.Layout = htmx.NewLayout("layout", BODY(
		DIV(

			modals,
			ID("short-body"),
			Class("has-background-dark"),
			DIV(
				Class("container is-fluid"),
				header,
				errors,
				content,
				footer,
			),
		),
	),
	)

	LAYOUT.Content = LAYOUT.Child("main-content", content)
	LAYOUT.Header = LAYOUT.Child("header", header)
	LAYOUT.Footer = LAYOUT.Child("footer", footer)
	LAYOUT.Modals = LAYOUT.Child("modals", modals)

}

func init() {
	initLayout()
}

const htmxConfig = `{"selfRequestsOnly":true,"allowScriptTags":true,"historyCacheSize":0,"getCacheBusterParam":true,"withCredentials":false}`

func Full(inner html.HTMLer) html.HTML {
	return HTML5(
		ATTR("data-theme", "light"),
		HEAD(
			CharsetUtf8(),
			Viewport("width=device-width, initial-scale=1"),
			META(attr.Name("htmx-config"), attr.Content(htmxConfig)),
			TITLE("shortcut manager"),
			CssHref(css.Join("pre-bulma.css").String()),
			CssHref(css.Join("bulma-1.0.3/bulma.min.css").String()),
			CssHref(css.Join("post-bulma.css").String()),
			CssHref(fontawesome5.Join("fontawesome.css").String()),
			CssHref(fontawesome5.Join("brands.css").String()),
			CssHref(fontawesome5.Join("solid.css").String()),
			JsSrc(js.Join("htmx-2.0.3/htmx.min.js").String()),
			JsSrc(js.Join("hyperscript-0.9.13/hyperscript.min.js").String()),
		),
		LAYOUT.Set(
			LAYOUTPos.Content.Set(inner),
		),
	).HTML()

}
