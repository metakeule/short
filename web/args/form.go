package args

import "gitlab.com/golang-utils/web/lib/wc"

var (
	FormName    = wc.NewFormArg("name").String()
	FormCommand = wc.NewFormArg("command").String()

	FormDefaultsKey   = wc.NewFormArg("defaults[key]").String()
	FormDefaultsValue = wc.NewFormArg("defaults[value]").String()
)
