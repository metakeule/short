package web

import (
	"embed"
	"fmt"
	"net/http"

	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/web/lib/router"
	"gitlab.com/metakeule/short/lib/short"
	"gitlab.com/metakeule/short/web/controller"
	"gitlab.com/metakeule/short/web/model"
	"gitlab.com/metakeule/static-junk-server/lib/server"
)

//go:embed static/*
var embedFS embed.FS

// if port is negative,  start with its absolute value and try the next 10 ports
func Run(st *short.FileStore, host string, port int) error {
	model.Store = st
	rt := router.New()
	rt.ServeEmbededFiles(embedFS, path.Relative("static/"))

	rt.NotFound(func(rw http.ResponseWriter, req *http.Request) {
		fmt.Printf("not found: %s\n", req.URL.Path)
		rw.WriteHeader(404)
	})

	controller.Router.Mount(rt, "/short")

	rt.GET("/", func(rw http.ResponseWriter, req *http.Request) {
		http.Redirect(rw, req, controller.List.Routes.GET.URL(), http.StatusSeeOther)
	})

	http.Handle("/", rt)

	if port < 0 {
		return server.RunOnFreePort(host, port*(-1), 10)
	} else {
		return server.Run(host, port)
	}
}
