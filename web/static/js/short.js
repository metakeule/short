

function onElementInserted(containerSelector, elementSelector, callback) {

    var onMutationsObserved = function(mutations) {
        mutations.forEach(function(mutation) {
            if (mutation.addedNodes.length) {
                var elements = $(mutation.addedNodes).find(elementSelector);
                for (var i = 0, len = elements.length; i < len; i++) {
                    callback(elements[i]);
                }
            }
        });
    };

    var target = $(containerSelector)[0];
    var config = { childList: true, subtree: true };
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    var observer = new MutationObserver(onMutationsObserved);    
    observer.observe(target, config);

}

function GetGeneralCategoriesForAssignment(targetElmId) {
  $.get("/ifks_intranet/ifks_text_tool/general_category/api/categories/", {}, function(data, status, jqXHR){
    let templateid = $(targetElmId).parent().attr("data-templateid");
    
    // generalcategoriesforassignment

    $(targetElmId).tree({
        data: data,
        autoOpen: 2,
        autoEscape: false,
        onCreateLi: function(node, $li) {
            // Append a link to the jqtree-element div.
            // The link has an url '#node-[id]' and a data property 'node-id'.

            // PUT /ifks_intranet/ifks_text_tool/template/api/put-text-assignment/"+ templateid + "/" + node.id
              $li.find('.jqtree-element').append(
                '<nav uk-dropnav="">' + 
                  '<ul class="uk-subnav ">' + 
                    '<li><a href="#"><span uk-drop-parent-icon=""></span></a>' + 
                      '<div class="uk-dropdown">' + 
                        '<ul class="uk-nav uk-navbar-dropdown-nav ">' + 
                          '<li><button class="uk-margin uk-button-small uk-icon" hx-headers=\'{"HX-Boosted": "true"}\' hx-swap="outerHTML" hx-target="body" hx-put="/ifks_intranet/ifks_text_tool/template/api/put-category-assignment/'+ templateid + '/' +  node.id +'" uk-icon="icon: plus">zuweisen</button></li>' +
                        '</ul>' +
                      '</div>' +
                    '</li>' + 
                  '</ul>' +
                '</nav>'
              );            
        }
    });

    htmx.process(targetElmId);       
  });
}


function GetGeneralCategories(targetElmId) {
  $.get("/ifks_intranet/ifks_text_tool/general_category/api/categories/", {}, function(data, status, jqXHR){
    
    $(targetElmId).tree({
        data: data,
        autoOpen: 2,
        autoEscape: false,
        onCreateLi: function(node, $li) {
            // Append a link to the jqtree-element div.
            // The link has an url '#node-[id]' and a data property 'node-id'.
            if (node.id === 2) {
              $li.find('.jqtree-element').append(
                '<nav uk-dropnav="">' + 
                  '<ul class="uk-subnav ">' + 
                    '<li><a href="#"><span uk-drop-parent-icon=""></span></a>' + 
                      '<div class="uk-dropdown">' + 
                        '<ul class="uk-nav uk-navbar-dropdown-nav ">' + 
                          '<li><a class="uk-margin " href="/ifks_intranet/ifks_text_tool/general_category/html/list-texts/' + node.id + '">Texte... <span uk-icon="icon: " class="uk-icon"> </span></a></li>' +                          
                          '<li><button class="uk-margin uk-button-small uk-icon" hx-swap="innerHTML" hx-target="#modals" hx-get="/ifks_intranet/ifks_text_tool/general_category/html/new/'+ node.id +'" uk-icon="icon: plus">neue Unterkategorie</button></li>' +
                        '</ul>' +
                      '</div>' +
                    '</li>' + 
                  '</ul>' +
                '</nav>'
              );
            } else {
              $li.find('.jqtree-element').append(
                  '<nav uk-dropnav="">' + 
                    '<ul class="uk-subnav ">' + 
                      '<li><a href="#"><span uk-drop-parent-icon=""></span></a>' + 
                        '<div class="uk-dropdown">' + 
                          '<ul class="uk-nav uk-navbar-dropdown-nav ">' + 
                            '<li><a class="uk-margin " href="/ifks_intranet/ifks_text_tool/general_category/html/list-texts/' + node.id + '">Texte... <span uk-icon="icon: " class="uk-icon"> </span></a></li>' +
                            '<li><button class="uk-margin uk-button-small uk-icon" hx-swap="innerHTML" hx-target="#modals" hx-get="/ifks_intranet/ifks_text_tool/general_category/html/edit/'+ node.id +'" uk-icon="icon: pencil">ändern</button></li>' +
                         //   '<li><button class="uk-margin uk-button-small uk-icon" hx-delete="/ifks_intranet/ifks_text_tool/general_category/api/delete/' + node.id + '" uk-icon="icon: trash" hx-target="'+targetElmId+'">löschen</button></li>' +
                            '<li><button class="uk-margin uk-button-small uk-icon" hx-swap="innerHTML" hx-target="#modals" hx-get="/ifks_intranet/ifks_text_tool/general_category/html/new/'+ node.id +'" uk-icon="icon: plus">neue Unterkategorie</button></li>' +
                          '</ul>' +
                        '</div>' +
                      '</li>' + 
                    '</ul>' +
                  '</nav>'
              );
          }
        }
    });

    htmx.process(targetElmId);       
  });
}

function GetCategoriesForDoc(docid, targetElmId) {
  $.get("/ifks_intranet/ifks_text_tool/document/api/categories/" + docid, {}, function(data, status, jqXHR){

    $(targetElmId).tree({
        data: data,
        autoOpen: 0,
        autoEscape: false
    });

    $(targetElmId).bind(
        'tree.click',
        function(event) {
          let node = event.node;
          htmx.ajax('GET','/ifks_intranet/ifks_text_tool/document/html/list_cat_texts/' + node.docid + '?category_id=' + node.id, '#listcardcattexts');          
        }
    );
  });
}

function GetCategoriesForTemplateEditing(targetElmId) {
  let templateid = $(targetElmId).attr("data-templateid");
  if (templateid == "") {
    console.log("could not find data-templateid attribute in " + targetElmId);
  }
  $.get("/ifks_intranet/ifks_text_tool/template/api/categories/" + templateid, {}, function(data, status, jqXHR){

    $(targetElmId).tree({
      data: data,
      autoOpen: 2,
      autoEscape: false,
      onCreateLi: function(node, $li) {
          // Append a link to the jqtree-element div.
          // The link has an url '#node-[id]' and a data property 'node-id'.
          if (node.id === 2) {
            $li.find('.jqtree-element').append(
              '<nav uk-dropnav="">' + 
                '<ul class="uk-subnav ">' + 
                  '<li><a href="#"><span uk-drop-parent-icon=""></span></a>' + 
                    '<div class="uk-dropdown">' + 
                      '<ul class="uk-nav uk-navbar-dropdown-nav ">' +                                         
                        '<li><button class="uk-margin uk-button-small uk-icon" hx-swap="innerHTML" hx-target="#modals" hx-get="/ifks_intranet/ifks_text_tool/template/html/new-category/'+ templateid + "/" + node.id +'" uk-icon="icon: plus">neue Unterkategorie</button></li>' +
                      '</ul>' +
                    '</div>' +
                  '</li>' + 
                '</ul>' +
              '</nav>'
            );
          } else {

            //htmx.ajax('GET','/ifks_intranet/ifks_text_tool/template/html/list_cat_texts/' + node.templateid + '?category_id=' + node.id, '#listcardcattexts');          

            $li.find('.jqtree-element').append(
                '<nav uk-dropnav="">' + 
                  '<ul class="uk-subnav ">' + 
                    '<li><a href="#"><span uk-drop-parent-icon=""></span></a>' + 
                      '<div class="uk-dropdown">' + 
                        '<ul class="uk-nav uk-navbar-dropdown-nav ">' + 
                          '<li><button class="uk-margin uk-button-small uk-icon" hx-get="/ifks_intranet/ifks_text_tool/template/html/list_cat_texts/' + node.templateid + '?category_id=' + node.id + '" hx-swap="innerHTML" uk-icon="icon: " hx-target="#listcardcattexts">Texte... </button></li>' +
                          '<li><button class="uk-margin uk-button-small uk-icon" hx-swap="innerHTML" hx-target="#modals" hx-get="/ifks_intranet/ifks_text_tool/template/html/rename-category/'+ templateid + "/" + node.id +'" uk-icon="icon: pencil">umbenennen</button></li>' +
                          '<li><button class="uk-margin uk-button-small uk-icon" hx-swap="innerHTML" hx-target="#modals" hx-get="/ifks_intranet/ifks_text_tool/template/html/new-category-text/'+ templateid + "/" + node.id +'" uk-icon="icon: plus">neuer Text</button></li>' +
                       //   '<li><button class="uk-margin uk-button-small uk-icon" hx-delete="/ifks_intranet/ifks_text_tool/general_category/api/delete/' + node.id + '" uk-icon="icon: trash" hx-target="'+targetElmId+'">löschen</button></li>' +
                          '<li><button class="uk-margin uk-button-small uk-icon" hx-swap="innerHTML" hx-target="#modals" hx-get="/ifks_intranet/ifks_text_tool/template/html/new-category/'+ templateid + "/" + node.id +'" uk-icon="icon: plus">neue Unterkategorie</button></li>' +
                        '</ul>' +
                      '</div>' +
                    '</li>' + 
                  '</ul>' +
                '</nav>'
            );
        }
      }
    });

    htmx.process(targetElmId);       


  });
}

function GetCategoriesForTemplate(templateid, targetElmId) {
  $.get("/ifks_intranet/ifks_text_tool/template/api/categories/" + templateid, {}, function(data, status, jqXHR){


    $(targetElmId).tree({
        data: data,
        autoOpen: 0,
        autoEscape: false
    });

    $(targetElmId).bind(
        'tree.click',
        function(event) {
          let node = event.node;
          htmx.ajax('GET','/ifks_intranet/ifks_text_tool/template/html/list_cat_texts/' + node.templateid + '?category_id=' + node.id, '#listcardcattexts');          
        }
    );
  });
}

var csrfToken = "";

$(document).ready(function(){
  $.get("/ifks_intranet/csrf", {}, function(data, status, jqXHR){
    csrfToken = jqXHR.getResponseHeader("X-CSRF-Token");
    //console.log("get: " + csrfToken);
  });

  onElementInserted('body', '#doccategories', function(element) {
    let docid = $(element).attr("data-docid");
    GetCategoriesForDoc(docid, "#doccategories");
  });

  onElementInserted('body', '#templatecategories', function(element) {
    let templateid = $(element).attr("data-templateid");
    GetCategoriesForTemplate(templateid, "#templatecategories");
  });

  onElementInserted('body', '#templateeditingcategories', function(element) {
    GetCategoriesForTemplateEditing("#templateeditingcategories");
  });

  if( $('#doccategories').length ) {
    let docid = $('#doccategories').attr("data-docid");
    GetCategoriesForDoc(docid, "#doccategories");
  }

  if( $('#templatecategories').length ) {
    let templateid = $('#templatecategories').attr("data-templateid");
    GetCategoriesForTemplate(templateid, "#templatecategories");
  }
  
  // if it exists
  if( $('#generalcategories').length ) {
    GetGeneralCategories( "#generalcategories");
  }

  if( $('#generalcategoriesforassignment').length ) {
    GetGeneralCategoriesForAssignment("#generalcategoriesforassignment");
  }

  if( $('#templateeditingcategories').length ) {
    GetCategoriesForTemplateEditing("#templateeditingcategories");
  }
  

  
  

  // see https://www.submitjson.com/blog/htmx-post-json
  htmx.defineExtension('submitjson', {
    onEvent: function (name, evt) {
      if (name === "htmx:configRequest") {
        evt.detail.headers['Content-Type'] = "application/json"
    //    evt.detail.headers['X-API-Key'] = 'sjk_xxx'
      }
    },
    encodeParameters: function(xhr, parameters, elt) {
      xhr.overrideMimeType('text/json') // override default mime type
      const body = { // set your request body
        data: parameters,
        options: { submissionFormat: 'pretty'}
      }
      return (JSON.stringify(body))
    }
  })
  
  
  document.body.addEventListener('loadGeneralCategories', function(evt) {
    GetGeneralCategories( "#generalcategories");
  });

  document.body.addEventListener('loadGeneralCategoriesForAssignment', function(evt) {
    GetGeneralCategoriesForAssignment("#generalcategoriesforassignment");
  });

  document.body.addEventListener('loadCategoriesForTemplateEditing', function(evt) {
    GetCategoriesForTemplateEditing("#templateeditingcategories");
  });

  
  

  //document.addEventListener('uikit:moved', () => {
  document.addEventListener('moved', (elm) => {
    let tbl = $(elm.target).parent("table")
    let divid = tbl.parent().attr("id");
    
    if (divid == "listcarddoctexts") {
        let docid = $(tbl.parent()).attr("data-docid");
        let neworder = "";
        $(tbl).find("tr").each(function( index, el ) {
            let no = $(this).find("td").first().text();

            if (no != "") {
                neworder = neworder + "," + no
            }            
        });

        htmx.ajax('PATCH','/ifks_intranet/ifks_text_tool/document/api/change_order/' + docid, {
            values: {
               order: neworder
            },
            target: '#listcarddoctexts',
            swap: "outerHTML"  
        });             
    }

    if (divid == "listcardtemplatetexts") {
      let templateid = $(tbl.parent()).attr("data-templateid");
      let neworder = "";
      $(tbl).find("tr").each(function( index, el ) {
          let no = $(this).find("td").first().text();

          if (no != "") {
              neworder = neworder + "," + no
          }            
      });

      htmx.ajax('PATCH','/ifks_intranet/ifks_text_tool/template/api/change_order/' + templateid, {
          values: {
             order: neworder
          },
          target: '#listcardtemplatetexts',
          swap: "outerHTML"  
      });             
  }
  });
  
  document.body.addEventListener('htmx:configRequest', function(evt) {
    //  evt.detail.parameters['auth_token'] = getAuthToken(); // add a new parameter into the request
    //console.log("set: " + csrfToken);
    evt.detail.headers["X-CSRF-Token"] = csrfToken;
  });

  document.body.addEventListener('htmx:targetError', function (evt) {
    console.log("target error: " + evt.detail.target);
  });

  document.body.addEventListener('htmx:beforeOnLoad', function (evt) {
   if (evt.detail.isError) {
        evt.detail.shouldSwap = true;
        evt.detail.isError = false;
   } else {
    $('#errors').empty();
   }
  });

  const i18n = {
      close: { label: 'schließen' },
      totop: { label: 'zum Anfang' },
      marker: { label: 'öffnen' },
      navbarToggleIcon: { label: 'Menü öffnen' },
      paginationPrevious: { label: 'vor' },
      paginationNext: { label: 'zurück' },
      slider: {
          next: 'vor',
          previous: 'zurück',
          slideX: 'Seite %s',
          slideLabel: '%s von %s',
      },
      slideshow: {
          next: 'vor',
          previous: 'zurück',
          slideX: 'Seite %s',
          slideLabel: '%s von %s',
      },
      lightboxPanel: {
          next: 'vor',
          previous: 'zurück',
          slideLabel: '%s von %s',
          close: 'schließen',
      },
  };

  for (const component in i18n) {
      UIkit.mixin({ i18n: i18n[component] }, component);
  }
});

